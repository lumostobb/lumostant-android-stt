package com.lumos.lusistant;

import android.Manifest;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.lumos.lusistant.tts.TextToSpeechManger;
import com.lumos.lusistant.tts.gcp.AudioConfig;
import com.lumos.lusistant.tts.gcp.EAudioEncoding;
import com.lumos.lusistant.tts.gcp.ESSMLlVoiceGender;
import com.lumos.lusistant.tts.gcp.GCPTTS;
import com.lumos.lusistant.tts.gcp.GCPTTSAdapter;
import com.lumos.lusistant.tts.gcp.GCPVoice;
import com.lumos.lusistant.tts.gcp.VoiceCollection;
import com.lumos.lusistant.tts.gcp.VoiceList;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private SpeechService speechService;
    private VoiceRecorder mVoiceRecorder;

    private TextToSpeechManger mTextToSpeechManger;
    private GCPTTS mGCPTTS;

    private TextView recordingTextView;

    private static final String TAG = "MainActivity";
    private static String finalText = "";

    private static String tarih_str = "";
    private static String etkinlik_str = "";

    private static int tarih = 0;
    private static int etkinlik = 0;
    public static String backendIP;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getBackendIP();

        //Record audio permission
        int permission = ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            int RECORD_AUDIO = 666;
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.RECORD_AUDIO},
                    RECORD_AUDIO);
        }

        //Internet permission
        permission = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_NETWORK_STATE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            int ACCESS_NETWORK_STATE = 333;
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_NETWORK_STATE},
                    ACCESS_NETWORK_STATE);
        }

        //Write external permission
        permission = ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if(permission != PackageManager.PERMISSION_GRANTED) {
            int WRITE_EXTERNAL_STORAGE = 1000;
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    WRITE_EXTERNAL_STORAGE);
        }

        Button startRecordingButton = findViewById(R.id.start_recording);
        Button stopRecordingButton = findViewById(R.id.stop_recording);
        Button startRecorderActivityButton = findViewById(R.id.startRecordActivity);
        recordingTextView = findViewById(R.id.recording_text_view);

        startRecordingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startVoiceRecorder();
            }
        });

        stopRecordingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopVoiceRecorder();
            }
        });

        startRecorderActivityButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent activity = new Intent(MainActivity.this, RecordAudioActivity.class);
                activity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                MainActivity.this.startActivity(activity);
            }
        });
        initGCPTTS();
    }

    @Override
    protected void onStart() {
        super.onStart();

        bindService(new Intent(this, SpeechService.class), mServiceConnection, BIND_AUTO_CREATE);
    }

    private final ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder binder) {
            speechService = SpeechService.from(binder);
            speechService.addListener(mSpeechServiceListener);
        }
        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            speechService = null;
        }
    };

    private final SpeechService.Listener mSpeechServiceListener =
            new SpeechService.Listener() {
                @Override
                public void onSpeechRecognized(final String text, final boolean isFinal) {
                    if (recordingTextView != null && !TextUtils.isEmpty(text)) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                recordingTextView.setText(text);
                                if(isFinal){
                                    Log.e(TAG, "Final text: " +  text);
                                    finalText = text;
                                }
                            }
                        });
                    }
                }
            };

    private final VoiceRecorder.Callback mVoiceCallback = new VoiceRecorder.Callback() {

        @Override
        public void onVoiceStart() {
            if (speechService != null) {
                speechService.startRecognizing(mVoiceRecorder.getSampleRate());
            }
        }

        @Override
        public void onVoice(byte[] data, int size) {
            if (speechService != null) {
                speechService.recognize(data, size);
            }
        }
    };

    private void startVoiceRecorder() {
        Log.e(TAG, "Recorder started...");
        if (mVoiceRecorder != null) {
            mVoiceRecorder.stop();
        }
        mVoiceRecorder = new VoiceRecorder(mVoiceCallback);
        mVoiceRecorder.start();
    }

    private void initGCPTTS() {

        final Context selfContext = this;
        VoiceList voiceList = new VoiceList();
        voiceList.addVoiceListener(new VoiceList.IVoiceListener() {
            @Override
            public void onResponse(String text) {
                JsonElement jsonElement = new JsonParser().parse(text);
                if (jsonElement == null || jsonElement.getAsJsonObject() == null ||
                        jsonElement.getAsJsonObject().get("voices").getAsJsonArray() == null) {
                    Log.e(TAG, "get error json");
                    return;
                }

                JsonObject jsonObject = jsonElement.getAsJsonObject();
                JsonArray jsonArray = jsonObject.get("voices").getAsJsonArray();
                final VoiceCollection voiceCollection = new VoiceCollection();
                GCPVoice gcpVoice = new GCPVoice("tr-TR", "tr-TR-Standard-A", ESSMLlVoiceGender.FEMALE, 24000);
                voiceCollection.add("tr-TR", gcpVoice);

                mGCPTTS = new GCPTTS();
                mGCPTTS.addSpeakListener(new GCPTTS.ISpeakListener() {
                    @Override
                    public void onSuccess(String message) {
                        Log.i(TAG, message);
                    }

                    @Override
                    public void onFailure(String errorMessage, String speakMessage) {
                        Log.e(TAG, "speak fail : " + errorMessage);
                    }
                });
            }

            @Override
            public void onFailure(String error) {
                mGCPTTS = null;
                Log.e(TAG, "Loading Voice List Error, error code : " + error);
            }
        });
        voiceList.start();
    }

    private TextToSpeechManger loadGCPTTS() {
        if (mGCPTTS == null) {
            return null;
        }

        String languageCode = "tr-TR";
        String name = "tr-TR-Standard-A";
        float pitch = ((float) 1.5);
        float speakRate = ((float) 1.2);

        GCPVoice gcpVoice = new GCPVoice(languageCode, name);
        AudioConfig audioConfig = new AudioConfig.Builder()
                .addAudioEncoding(EAudioEncoding.MP3)
                .addSpeakingRate(speakRate)
                .addPitch(pitch)
                .build();

        mGCPTTS.setGCPVoice(gcpVoice);
        mGCPTTS.setAudioConfig(audioConfig);
        GCPTTSAdapter gcpttsAdapter = new GCPTTSAdapter(mGCPTTS);

        return new TextToSpeechManger(gcpttsAdapter);
    }

    private void stopVoiceRecorder() {
        Log.e(TAG, "Recorder stopped...");
        if (mVoiceRecorder != null) {
            mVoiceRecorder.stop();
            mVoiceRecorder = null;
        }
        if (speechService != null) {
            speechService.finishRecognizing();
        }

        if (mTextToSpeechManger != null) {
            mTextToSpeechManger.stop();
        }

        mTextToSpeechManger = loadGCPTTS();
        if(finalText.length() > 0){
            Log.e(TAG, "trying to speak");
            try {
                processTheVoice(finalText);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    private void processTheVoice(String finalText) throws ParseException, IOException, JSONException {
        finalText = finalText.toLowerCase();

        if(etkinlik == 1){
            etkinlik_str = finalText;
            mTextToSpeechManger.speak("Tarih ve saati öğrenebilir miyim?");
            tarih = 1;
            etkinlik = 0;
        }
        else if(tarih == 1){
            tarih_str = finalText;

            tarih = 0;


            String tarih_tmp = tarih_str;
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss", new Locale("tr"));

            String gun = tarih_tmp.substring(0, tarih_tmp.indexOf(" "));
            tarih_tmp = tarih_tmp.substring(tarih_tmp.indexOf(" ") + 1);

            String ay = tarih_tmp.substring(0, 3);
            tarih_tmp = tarih_tmp.substring(tarih_tmp.indexOf(" ") + 1);
            ay = ay.substring(0, 1).toUpperCase() + ay.substring(1);

            String yil = tarih_tmp.substring(0, tarih_tmp.indexOf(" "));
            tarih_tmp = tarih_tmp.substring(tarih_tmp.indexOf(" ") + 1);
            String saat = "";
            String dakika = "";
            System.out.println(gun + ", " + ay + ", " + yil);
            if(tarih_tmp.indexOf(".") == -1){
                if(tarih_tmp.trim().indexOf(" ") != -1){
                    saat = tarih_tmp.substring(0, tarih_tmp.indexOf(" "));
                    dakika = tarih_tmp.substring(tarih_tmp.indexOf(" ") + 1);
                }
            }
            else{
                saat = tarih_tmp.substring(0, tarih_tmp.indexOf("."));
                dakika = tarih_tmp.trim().substring(tarih_tmp.indexOf(".") + 1);
            }

            String saniye = ":00";

            String date_form = gun + "-" + ay + "-" + yil + " " + saat + ":" + dakika + saniye;

            Date date_o1d = sdf.parse(date_form);

            SimpleDateFormat sdf_last = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", new Locale("tr"));

            String d2 = sdf_last.format(date_o1d);
            Date d2_new = sdf_last.parse(d2);

            System.out.println("********" + d2);

            postNewEvent(d2, "CanT1", etkinlik_str);


            etkinlik_str = "";
            tarih_str = "";
        }
        else if(finalText.indexOf("etkinlik") != -1 && finalText.indexOf("ekle") != -1) {
            System.out.println("SA");
            mTextToSpeechManger.speak("Etkinlik adını söyler misin?");
            etkinlik = 1;
        }
        else {
            String command = finalText;
            mTextToSpeechManger.speak("Lütfen bekleyiniz");
            postNewCommand(command);
            finalText = "";
        }

        finalText = "";
    }

    @Override
    protected void onStop() {
        //stopVoiceRecorder();

        speechService.removeListener(mSpeechServiceListener);
        unbindService(mServiceConnection);
        speechService = null;

        super.onStop();
    }

    public void postNewCommand(final String text){
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    System.out.println("objects fecthing");

                    final String addition = text.replaceAll(" ", "%20");


                    String addition_clean = URLEncoder.encode(addition, "utf-8");

                    URL getEventsUrl;

                    if(addition.contains("hava")){
                        getEventsUrl = new URL("http://" + backendIP + ":8080/runWeather");
                    }
                    else{
                        getEventsUrl = new URL("http://" + backendIP + ":8080/runTask/" + addition_clean);
                    }

                    HttpURLConnection httpURLConnection = (HttpURLConnection) getEventsUrl.openConnection();
                    InputStream inputStream = httpURLConnection.getInputStream();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                    StringBuilder builder = new StringBuilder();

                    String line = "";
                    while (line != null) {
                        line = bufferedReader.readLine();
                        builder.append(line);
                    }

                    String result = builder.toString();

                    result = result.substring(0, result.indexOf("null"));

                    System.out.println("+++++-----+++++" + result);
                    mTextToSpeechManger.speak(result);
                } catch (MalformedURLException e) {
                    mTextToSpeechManger.speak("işlem gerçekleştirilemedi.");
                    e.printStackTrace();
                } catch (IOException e) {
                    mTextToSpeechManger.speak("işlem gerçekleştirilemedi.");
                    e.printStackTrace();
                }
            }
        }).start();

    }

    public void postNewEvent(final String d1, final String username, final String eventName) throws IOException, JSONException {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    URL url = new URL("http://" + backendIP + ":8080/addEvent");
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestMethod("POST");
                    conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
                    conn.setRequestProperty("Accept", "application/json");
                    conn.setDoOutput(true);
                    conn.setDoInput(true);

                    JSONObject jsonParam = new JSONObject();
                    jsonParam.put("username", username);
                    jsonParam.put("eventDate", d1);
                    jsonParam.put("eventName", eventName);

                    String json = jsonParam.toString();

                    byte[] outputInBytes = json.getBytes("UTF-8");
                    OutputStream os = conn.getOutputStream();
                    os.write( outputInBytes );
                    os.close();

                    int responseCode = conn.getResponseCode();

                    System.out.println("Response code: " + responseCode );

                    Log.i("JSON", jsonParam.toString());


                    /*
                    DataOutputStream os = new DataOutputStream(conn.getOutputStream());
                    //os.writeBytes(URLEncoder.encode(jsonParam.toString(), "UTF-8"));
                    os.writeBytes(jsonParam.toString());

                    os.flush();
                    os.close();
                    */

                    Log.i("STATUS", String.valueOf(conn.getResponseCode()));
                    Log.i("MSG", conn.getResponseMessage());

                    conn.disconnect();

                    mTextToSpeechManger.speak("Etkinlik kaydedildi!");

                } catch (IOException e) {
                    e.printStackTrace();
                    mTextToSpeechManger.speak("Etkinlik eklenemedi!");
                } catch (JSONException e) {
                    e.printStackTrace();
                    mTextToSpeechManger.speak("Etkinlik eklenemedi!");
                }
            }
        }).start();

    }


    public void getBackendIP(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    URL getEventsUrl = new URL("https://lumos-ip.herokuapp.com/ip");

                    HttpURLConnection httpURLConnection = (HttpURLConnection) getEventsUrl.openConnection();
                    InputStream inputStream = httpURLConnection.getInputStream();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                    StringBuilder builder = new StringBuilder();

                    String line = "";
                    while (line != null) {
                        line = bufferedReader.readLine();
                        builder.append(line);
                    }

                    String result = builder.toString();

                    backendIP = result.split(":")[1].substring(0, result.split(":")[1].indexOf('n'));

                } catch (MalformedURLException e) {
                    mTextToSpeechManger.speak("Backende bağlanılamadı");
                    e.printStackTrace();
                } catch (IOException e) {
                    mTextToSpeechManger.speak("Backende bağlanılamadı");
                    e.printStackTrace();
                }
            }
        }).start();

    }

}
