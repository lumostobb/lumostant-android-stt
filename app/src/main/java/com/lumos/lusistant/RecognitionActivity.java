package com.lumos.lusistant;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.lumos.lusistant.tts.TextToSpeechManger;
import com.lumos.lusistant.tts.gcp.AudioConfig;
import com.lumos.lusistant.tts.gcp.EAudioEncoding;
import com.lumos.lusistant.tts.gcp.ESSMLlVoiceGender;
import com.lumos.lusistant.tts.gcp.GCPTTS;
import com.lumos.lusistant.tts.gcp.GCPTTSAdapter;
import com.lumos.lusistant.tts.gcp.GCPVoice;
import com.lumos.lusistant.tts.gcp.VoiceCollection;
import com.lumos.lusistant.tts.gcp.VoiceList;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import AlizeSpkRec.AlizeException;
import AlizeSpkRec.SimpleSpkDetSystem;

public class RecognitionActivity extends AppCompatActivity {

    final Handler handler = new Handler();

    private Button recognizeMeButton;
    private SimpleSpkDetSystem alizeSystem;
    private MediaRecorder myAudioRecorder;
    private String outputFile;

    private static final String TAG = "RecognitionActivity";

    private TextToSpeechManger mTextToSpeechManger;
    private GCPTTS mGCPTTS;

    public static String backendIP;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recognition);

        getBackendIP();

        initGCPTTS();

        //Write external permission
        int permission = ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if(permission != PackageManager.PERMISSION_GRANTED) {
            int WRITE_EXTERNAL_STORAGE = 1000;
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    WRITE_EXTERNAL_STORAGE);
        }

        //Record audio permission
        permission = ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            int RECORD_AUDIO = 666;
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.RECORD_AUDIO},
                    RECORD_AUDIO);
        }

        try {
            InputStream configAsset = getApplicationContext().getAssets().open("AlizeDefault.cfg");
            alizeSystem = new SimpleSpkDetSystem(configAsset, getApplicationContext().getFilesDir().getPath());
            configAsset.close();

            InputStream backgroundModelAsset = getApplicationContext().getAssets().open("gmm/spk01.gmm");
            alizeSystem.loadBackgroundModel(backgroundModelAsset);
            backgroundModelAsset.close();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (AlizeException e) {
            e.printStackTrace();
        }

        recognizeMeButton = findViewById(R.id.recognize_me);

        recognizeMeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mTextToSpeechManger != null) {
                    mTextToSpeechManger.stop();
                }

                mTextToSpeechManger = loadGCPTTS();
                mTextToSpeechManger.speak("Lütfen Lucy beni tanı cümlesini 3 kere tekrar ediniz");

                try {
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            myAudioRecorder = new MediaRecorder();
                            myAudioRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
                            myAudioRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
                            myAudioRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
                            outputFile = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "Lusistant" + File.separator + "recognition.3gp";
                            myAudioRecorder.setOutputFile(outputFile);
                            try {
                                myAudioRecorder.prepare();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            myAudioRecorder.start();
                        }
                    }, 3800);

                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            myAudioRecorder.stop();
                            myAudioRecorder.reset();
                            myAudioRecorder.release();
                            myAudioRecorder = null;
                            Thread thread = new Thread(new Runnable(){
                                @Override
                                public void run() {
                                    try {
                                        uploadMedia();
                                        speakerRecognition();
                                    } catch (Exception e) {
                                        Log.e(TAG, e.getMessage());
                                    }
                                }
                            });
                            thread.start();
                        }
                    }, 10000);


                } catch (IllegalStateException ise) {
                    Log.e(TAG, "Start failed");
                }
            }
        });
    }

    private void initGCPTTS() {

        final Context selfContext = this;
        VoiceList voiceList = new VoiceList();
        voiceList.addVoiceListener(new VoiceList.IVoiceListener() {
            @Override
            public void onResponse(String text) {
                JsonElement jsonElement = new JsonParser().parse(text);
                if (jsonElement == null || jsonElement.getAsJsonObject() == null ||
                        jsonElement.getAsJsonObject().get("voices").getAsJsonArray() == null) {
                    Log.e(TAG, "get error json");
                    return;
                }

                final VoiceCollection voiceCollection = new VoiceCollection();
                GCPVoice gcpVoice = new GCPVoice("tr-TR", "tr-TR-Standard-A", ESSMLlVoiceGender.FEMALE, 24000);
                voiceCollection.add("tr-TR", gcpVoice);

                mGCPTTS = new GCPTTS();
                mGCPTTS.addSpeakListener(new GCPTTS.ISpeakListener() {
                    @Override
                    public void onSuccess(String message) {
                        Log.i(TAG, message);
                    }

                    @Override
                    public void onFailure(String errorMessage, String speakMessage) {
                        Log.e(TAG, "speak fail : " + errorMessage);
                    }
                });
            }

            @Override
            public void onFailure(String error) {
                mGCPTTS = null;
                Log.e(TAG, "Loading Voice List Error, error code : " + error);
            }
        });
        voiceList.start();
    }

    private TextToSpeechManger loadGCPTTS() {
        if (mGCPTTS == null) {
            return null;
        }

        String languageCode = "tr-TR";
        String name = "tr-TR-Standard-A";
        float pitch = ((float) 1.5);
        float speakRate = ((float) 1.2);

        GCPVoice gcpVoice = new GCPVoice(languageCode, name);
        AudioConfig audioConfig = new AudioConfig.Builder()
                .addAudioEncoding(EAudioEncoding.MP3)
                .addSpeakingRate(speakRate)
                .addPitch(pitch)
                .build();

        mGCPTTS.setGCPVoice(gcpVoice);
        mGCPTTS.setAudioConfig(audioConfig);
        GCPTTSAdapter gcpttsAdapter = new GCPTTSAdapter(mGCPTTS);

        return new TextToSpeechManger(gcpttsAdapter);
    }

    public void getBackendIP(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    URL getEventsUrl = new URL("https://lumos-ip.herokuapp.com/ip");

                    HttpURLConnection httpURLConnection = (HttpURLConnection) getEventsUrl.openConnection();
                    InputStream inputStream = httpURLConnection.getInputStream();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                    StringBuilder builder = new StringBuilder();

                    String line = "";
                    while (line != null) {
                        line = bufferedReader.readLine();
                        builder.append(line);
                    }

                    String result = builder.toString();

                    backendIP = result.split(":")[1].substring(0, result.split(":")[1].indexOf('n'));

                    System.out.println("IP: " + backendIP);

                } catch (MalformedURLException e) {
                    mTextToSpeechManger.speak("Backende bağlanılamadı");
                    e.printStackTrace();
                } catch (IOException e) {
                    mTextToSpeechManger.speak("Backende bağlanılamadı");
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public void speakerRecognition(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    URL getEventsUrl = new URL("http://" + backendIP + ":8080/speakerRecognition");

                    HttpURLConnection httpURLConnection = (HttpURLConnection) getEventsUrl.openConnection();
                    InputStream inputStream = httpURLConnection.getInputStream();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                    StringBuilder builder = new StringBuilder();

                    String line = "";
                    while (line != null) {
                        line = bufferedReader.readLine();
                        builder.append(line);
                    }

                    String result = builder.toString();

                    result = result.substring(0, result.indexOf("null"));

                    if((result.equals("furkan") || result.equals("can") || result.equals("benan"))){
                        mTextToSpeechManger.speak("Hoşgeldin " + result);
                        Intent activity = new Intent(RecognitionActivity.this, MainActivity.class);
                        activity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                        RecognitionActivity.this.startActivity(activity);
                    }
                    else{
                        mTextToSpeechManger.speak("Özür dilerim sizi tanıyamadım");
                    }


                } catch (MalformedURLException e) {
                    mTextToSpeechManger.speak("Backende bağlanılamadı");
                    e.printStackTrace();
                } catch (IOException e) {
                    mTextToSpeechManger.speak("Backende bağlanılamadı");
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private void uploadMedia() {
        try {

            String charset = "UTF-8";
            File uploadFile1 = new File(outputFile);
            String requestURL = "http://" + backendIP + ":8080/upload";

            MultipartUtility multipart = new MultipartUtility(requestURL, charset);

            multipart.addFilePart("file", uploadFile1);

            List<String> response = multipart.finish();

            Log.v("rht", "SERVER REPLIED:");

            for (String line : response) {
                Log.v("rht", "Line : "+line);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
