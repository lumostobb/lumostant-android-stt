package com.lumos.lusistant;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.media.MediaRecorder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.util.Base64;

import org.apache.commons.io.FileUtils;
import org.json.JSONObject;


public class RecordAudioActivity extends FragmentActivity {

    private Button stop, record;
    private EditText recorderName;
    private MediaRecorder myAudioRecorder;
    private String outputFile;

    private static final String TAG = "RecordAudioActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.record_voice_layout);
        stop = findViewById(R.id.stop);
        record = findViewById(R.id.record);
        stop.setEnabled(false);

        recorderName = findViewById(R.id.recorder_name);

        int permission = ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if(permission != PackageManager.PERMISSION_GRANTED) {
            int WRITE_EXTERNAL_STORAGE = 1000;
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    WRITE_EXTERNAL_STORAGE);
        }

        File folder = new File(Environment.getExternalStorageDirectory() +
                File.separator + "Lusistant");
        if (!folder.exists()) {
            folder.mkdirs();
        }

        record.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    myAudioRecorder = new MediaRecorder();
                    myAudioRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
                    myAudioRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
                    myAudioRecorder.setAudioEncoder(MediaRecorder.OutputFormat.THREE_GPP);
                    outputFile = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "Lusistant" + File.separator + recorderName.getText() + ".3gp";
                    System.out.println("***" + outputFile);
                    myAudioRecorder.setOutputFile(outputFile);
                    myAudioRecorder.prepare();
                    myAudioRecorder.start();
                } catch (IllegalStateException ise) {
                    Log.e(TAG, "Start failed");
                } catch (IOException ioe) {
                    Log.e(TAG, "Start failed" + ioe.getMessage());
                }
                record.setEnabled(false);
                stop.setEnabled(true);
                Toast.makeText(getApplicationContext(), "Recording started", Toast.LENGTH_LONG).show();
            }
        });

        stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myAudioRecorder.stop();
                myAudioRecorder.reset();
                myAudioRecorder.release();
                myAudioRecorder = null;
                record.setEnabled(true);
                stop.setEnabled(false);
                Toast.makeText(getApplicationContext(), "Audio Recorder successfully", Toast.LENGTH_LONG).show();
                try {
                    String encoded = encodeFileToBase64Binary(outputFile);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Thread thread = new Thread(new Runnable(){
                    @Override
                    public void run() {
                        try {
                            uploadMedia();
                        } catch (Exception e) {
                            Log.e(TAG, e.getMessage());
                        }
                    }
                });
                thread.start();

            }
        });
    }

    private void uploadMedia() {
        try {

            String charset = "UTF-8";
            File uploadFile1 = new File(outputFile);
            String requestURL = "http://" + MainActivity.backendIP + ":8080/upload";

            MultipartUtility multipart = new MultipartUtility(requestURL, charset);

            multipart.addFilePart("file", uploadFile1);

            List<String> response = multipart.finish();

            Log.v("rht", "SERVER REPLIED:");

            for (String line : response) {
                Log.v("rht", "Line : "+line);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private static String encodeFileToBase64Binary(String fileName)
            throws IOException {

        int flags = Base64.NO_WRAP | Base64.URL_SAFE;

        File file = new File(fileName);

        byte[] bytes = FileUtils.readFileToByteArray(file);

        String encoded = Base64.encodeToString(bytes, 0);

        //sendPost(encoded);

        /*byte[] decoded = Base64.decode(encoded, 0);
        System.out.println("Decoded: " + Arrays.toString(decoded));

        try
        {
            File file2 = new File(Environment.getExternalStorageDirectory() + "/hello-5.wav");
            FileOutputStream os = new FileOutputStream(file2, true);
            os.write(decoded);
            os.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }*/

        return encoded;
    }


    public static void sendPost(final String audioFile) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    URL url = new URL("http://" + MainActivity.backendIP + ":8080/audio");
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestMethod("POST");
                    conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
                    conn.setRequestProperty("Accept", "application/json");
                    conn.setDoOutput(true);
                    conn.setDoInput(true);

                    /*
                    HashMap<String, String> params = new HashMap<>();
                    params.put("audio", audioFile);
                    */


                    JSONObject jsonParam = new JSONObject();

                    jsonParam.put("audio", audioFile);


                    Log.i("JSON", jsonParam.toString());
                    DataOutputStream os = new DataOutputStream(conn.getOutputStream());
                    //os.writeBytes(URLEncoder.encode(jsonParam.toString(), "UTF-8"));
                    os.writeBytes(jsonParam.toString());

                    os.flush();
                    os.close();


                    Log.i("STATUS", String.valueOf(conn.getResponseCode()));
                    Log.i("MSG", conn.getResponseMessage());

                    conn.disconnect();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
    }
    /*private static byte[] loadFile(File file) throws IOException {

        InputStream is = new FileInputStream(file);

        long length = file.length();
        if (length > Integer.MAX_VALUE) {
            // File is too large
        }
        byte[] bytes = new byte[(int)length];

        int offset = 0;
        int numRead = 0;
        while (offset < bytes.length
                && (numRead=is.read(bytes, offset, bytes.length-offset)) >= 0) {
            offset += numRead;
        }

        if (offset < bytes.length) {
            throw new IOException("Could not completely read file "+file.getName());
        }

        is.close();
        return bytes;
    }

*/

}
